<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        return view ('home1');
    }

    public function product($username)
    {
        $products = ['name'=>'iphone', 'price'=>799];
        return view ('product',compact('products'));
    }

    public function create () {
        //show the form
        return view ('product.create');
    }

    public function store (Request $request) {
        //submit the form
        $this->validate(request(), [
            'title'=>'required|min:3|max:8',
            'description'=> 'required'
        ],[
            'title.required'=> 'El valor no puede quedar vacio',
            'title.min'=> 'El valor debe tener al menos 3 caracteres',
            'title.max'=> 'El valor no debe tener mas de 8 caracteres',
        ]);

        return back()->with('message', 'Tu producto fue agregado');

    }

}
