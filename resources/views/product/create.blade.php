<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    @if(Session::has('message'))
        <p>{{Session::get('message')}}</p>
    @endif

    @if($errors->any())
    <ul>
        @foreach($errors->all() as $e)
            <li>{{$e}}</li>
        @endforeach
    </ul>
    @endif
    <form action="{{route ('store')}}" method="post">
        @csrf
        
        <input type="text" name="title" placeholder="titulo del producto"
        value="{{old('title')}}"> 
        @error('title')
            <p>{{ $message }}</p>
        @enderror

        <textarea name="description" placeholder=""></textarea>
        @error('description')
            <p>{{ $message }}</p>
        @enderror

        <button type="submit">Continuar</button>
    </form>
</body>
</html>